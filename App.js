import React, { useState } from 'react';
import { Grid, Button } from '@mui/material';

const MyHouse = () => {
  const [isWindowOpen, setIsWindowOpen] = useState(false);
  const [isDoorOpen, setIsDoorOpen] = useState(false);

  const toggleWindow = () => {
    setIsWindowOpen(!isWindowOpen);
    alert(isWindowOpen ? "Window is closed!" : "Window is opened!");
  };

  const toggleDoor = () => {
    setIsDoorOpen(!isDoorOpen);
    alert(isDoorOpen ? "Door is closed!" : "Door is opened!");
  };

  return (
    <div style={{ display: "flex", justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Grid container direction='column' style={{ backgroundColor: 'green', height: '60%', width: '50%' }}>
        <Grid item style={{ backgroundColor: 'red', height: '50%' }}>
          <Grid container direction='row' style={{ backgroundColor: 'red', height: '150%', width: '100%', display: 'flex', justifyContent: 'right', alignItems: 'center' }}>
            <Grid item xs={6} style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center'}}>
              {/* Window Left */}
              <div style={{ backgroundColor: 'blue', height: '120%', width: '20%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                {isWindowOpen ? 'Open Window' : 'Close Window'}
              </div>
            </Grid>
            <Grid item xs={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
              {/* Window Right */}
              <div style={{ backgroundColor: 'yellow', height: '150%', width: '20%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                {isWindowOpen ? 'Open Window' : 'Close Window'}
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ backgroundColor: 'red', height: '50%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          {/* Door - Vertical */}
          <Grid container direction='row' style={{ backgroundColor: 'red', height: '100%' }}>
            <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <div style={{ backgroundColor: 'green', height: '95%', width: '25%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                {isDoorOpen ? 'Open Door' : 'Close Door'}
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '20px' }}>
          <Button variant="contained" onClick={toggleWindow} style={{ marginRight: '10px' }}>
            {isWindowOpen ? 'Close Window' : 'Open Window'}
          </Button>
          <Button variant="contained" onClick={toggleDoor}>
            {isDoorOpen ? 'Close Door' : 'Open Door'}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default MyHouse;
